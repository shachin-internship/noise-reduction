#include<opencv2/imgcodecs.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui.hpp>

#include<iostream>
#include<vector>
#include<cstdlib>

using namespace cv;
using namespace std;

// void insertionSort(std::vector<int> &vec)
// {
//     for (auto i = vec.begin(); i != vec.end(); i++)
//     {        
        
//         auto const insertion_point = 
//                 upper_bound(vec.begin(), i, *i);
          
//         rotate(insertion_point, i, i+1);        
//     }
// }


void medianFiltering(Mat image,Mat clone_img, const int kernelSize , int stride)
{    

	int imageChannels = image.channels();

	vector<vector<int>> values (imageChannels);
    
    int m = kernelSize / 2;

	int mid = kernelSize * kernelSize / 2 ;

	for (int x = m; x < image.rows - m; x++)
	{
		for (int y = m; y < image.cols - m; y+=stride)
		{
			for (int channel = 0; channel < imageChannels; channel++)
			{

				values[channel].clear();
			} 

			for (int i = -m; i <= m; i++)
			{
				for (int j = -m; j <= m; j++)
				{
					for (int channel = 0; channel < imageChannels; channel++)
					{
						unsigned char * pixelValuePtr = image.ptr(i + x) + ((j + y) * imageChannels) + channel;
						values[channel].push_back(*pixelValuePtr);
						
					}
				}
			}

			for (int channel = 0; channel < imageChannels; channel++)
			{   
<<<<<<< HEAD
				sort(begin(values[channel]), end(values[channel]));
=======
				insertionSort(values[channel]);
>>>>>>> 2a1b047e00326b769d647677c762f5f3b09d43e9
                unsigned char * val_ptr = clone_img.ptr(x) + (y * imageChannels) + channel;
			    *val_ptr = values[channel][mid];
				
				
			}
		}
	}


    
}



int main(int argc ,  char* argv[])
{
    Mat image, clone_img;
    string image_name = argv[1];
    int dimension;
    dimension = atoi(argv[2]);
    int stride = atoi(argv[3]);
    image = imread(image_name);

    if( !image.data )
    { return -1; }
    clone_img = image.clone();
<<<<<<< HEAD
    clock_t start_time = clock();
	medianFiltering(image ,clone_img ,  dimension , stride);

	double elapsed_time = (double)(clock() - start_time) / CLOCKS_PER_SEC;
	double ms = elapsed_time * 1000;
	cout << "\n";
    cout << "Done in %.2f ms\n" << ms;
=======
    medianFiltering(image ,clone_img ,  dimension , stride);
>>>>>>> 2a1b047e00326b769d647677c762f5f3b09d43e9
    imshow("initial" , image);
    imshow("final", clone_img);
        
    waitKey();
    return 0;
}