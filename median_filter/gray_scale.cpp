#include<opencv2/imgcodecs.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui.hpp>
#include<iostream>
#include<cstdlib>
using namespace cv;
using namespace std;
void insertion_sort(int arr[] , int m)
{
    int i , j , temp;
    for(i = 0 ; i < m ; i++){
        temp = arr[i];
        for(j = i - 1 ; j >= 0 && temp < arr[j];j--){
        arr[j+1] = arr[j];
        }
        arr[j+1] = temp;
    }
}
void median(Mat image , Mat clone_image , int dimension , int stride)
{
        int a = dimension * dimension;
        int window[a];
        int k;
        int m = dimension / 2;
        
        for(int x = m; x < image.rows - 1; x++){
            for(int y = m; y < image.cols - 1; y+=stride){
                // Pick up window element
                k = 0;
                for(int i = -m ; i <= m ; i++)
                {
                    for(int j = -m ; j<= m ; j++)
                    {
                        window[k++] = image.at<uchar>(x + i , y + j);
                    }
                }
                insertion_sort(window , a);
                clone_image.at<uchar>(x,y) = window[a / 2];
            }
        }
        namedWindow("final");
        imshow("final", clone_image);
        namedWindow("initial");
        imshow("initial", image);
      waitKey();
    imshow("clone_image" , image);
}
int main(int argc ,  char* argv[])
{
    Mat image, clone_image;
    string image_name = argv[1];
    int dimension ,stride ;
    dimension = atoi(argv[2]);
    stride = atoi(argv[3]);
    image = imread(image_name, IMREAD_GRAYSCALE);
    if( !image.data )
    { return -1; }
    clone_image = image.clone();
    median(image , clone_image , dimension , stride);
    return 0;
}