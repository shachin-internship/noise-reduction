import cv2
import sys
import time

image_name = sys.argv[1]
img = cv2.imread(image_name)
start_time = time.time()
median = cv2.medianBlur(img , int(sys.argv[2]))
print("--- {0:.2f} ms ---" .format ((time.time() - start_time) * 1000))
cv2.imshow("final"  , median)
cv2.waitKey(0)
cv2.destroyAllWindows()