import cv2
import sys
image_name = sys.argv[1]
img = cv2.imread(image_name)
median = cv2.medianBlur(img , int(sys.argv[2]))
cv2.imshow("final"  , median)
cv2.waitKey(0)
cv2.destroyAllWindows()